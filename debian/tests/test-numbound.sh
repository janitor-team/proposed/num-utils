#!/bin/bash

testupperbound(){
	for i in $myfiles; do
	        numbound $AUTOPKGTEST_TMP/tests/$i            | grep $myret
        	cat      $AUTOPKGTEST_TMP/tests/$i | numbound | grep $myret
	done
}


testlowerbound(){
	for i in $myfiles; do
	        numbound -l $AUTOPKGTEST_TMP/tests/$i       | grep $myret
        	cat $AUTOPKGTEST_TMP/tests/$i | numbound -l | grep $myret
	done
}


#######################################
##### Perform tests with numbound #####
#######################################

# step #1.a. files: columns, columns2
myret=5
myfiles="columns columns2"

testupperbound


# step #1.b.
myret=1

testlowerbound


# step #2.a. file: fractionalnums
myret=10241
myfiles="fractionalnums"

testupperbound


# step #2.b.
myret="10.5"

testlowerbound


# step #3.a. file: numbers
myret=5135
myfiles="numbers"

testupperbound


# step #4.b:
myret="2.543"

testlowerbound


# step #5.a. file: numbers2
myret=2542
myfiles="numbers2"

testupperbound


# step #5.b
myret=1

testlowerbound


# step #6.a. file: zeros
myret="0001.3515"
myfiles=zeros

testupperbound


# step #6.b.
testlowerbound
