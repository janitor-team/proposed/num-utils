#!/bin/bash

for i in $(seq 1 5); do
	numprocess /\*$i/ $AUTOPKGTEST_TMP/tests/nproc1 | numgrep /m$i/ | wc -l | grep "${i}"
done
