#!/bin/bash

myret=0;

for max in $(seq 1 10); do
	head -n${max} $AUTOPKGTEST_TMP/tests/sums | tail -n${max} | numsum || let myret=$myret+1
done

if [ "$myret" -gt "0" ]; then
	false
fi
