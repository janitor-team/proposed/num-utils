#!/bin/bash

### copy tests from upstream to $AUTOPKGTEST_TMP
###cp debian/num-utils/usr/share/doc/num-utils/examples/* $AUTOPKGTEST_TMP

# test commands with -h, -help, --help without any other parameters
# Must print 'Usage:'
for cmd in numaverage numinterval numprocess numrange numsum numbound numgrep numnormalize numrandom numround; do 
	$cmd  -h    2>/dev/null | grep Usage; 
	$cmd  -help 2>/dev/null | grep Usage;
	$cmd --help 2>/dev/null | grep Usage;
done

# test commands with -h, -help, --help with any other parameters
# Must run help() and print only 'Usage:'
for cmd in numaverage numinterval numprocess numrange numsum numbound numgrep numnormalize numrandom numround; do 
	$cmd -iMld $AUTOPKGTEST_TMP/tests/columns  -h    2>/dev/null | grep Usage;
	$cmd -imd  $AUTOPKGTEST_TMP/tests/columns2 -help 2>/dev/null | grep Usage;
	$cmd --help 2>/dev/null | grep Usage;
done
