#!/bin/bash

# test numaverage

myret=1;
for i in -im -i -iM -iMl; do
        numaverage $i $AUTOPKGTEST_TMP/tests/columns          | grep $myret
        cat $AUTOPKGTEST_TMP/tests/columns | numaverage $i    | grep $myret;

        numaverage -l $i $AUTOPKGTEST_TMP/tests/columns       | grep $myret;
        cat $AUTOPKGTEST_TMP/tests/columns | numaverage -l $i | grep $myret;

	myret=3;
done


for i in -I -Im -IM -IMl ; do
        numaverage $i $AUTOPKGTEST_TMP/tests/columns          | grep 0;
        cat $AUTOPKGTEST_TMP/tests/columns | numaverage $i    | grep 0;

        numaverage -l $i $AUTOPKGTEST_TMP/tests/columns       | grep 0;
        cat $AUTOPKGTEST_TMP/tests/columns | numaverage -l $i | grep 0;
done
