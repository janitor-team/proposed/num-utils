#!/bin/bash

# autopkgtest for num-utils

$AUTOPKGTEST_TMP/tests/test-help.sh
$AUTOPKGTEST_TMP/tests/test-numavg.sh
$AUTOPKGTEST_TMP/tests/test-numbound.sh
$AUTOPKGTEST_TMP/tests/test-numgrep.sh
$AUTOPKGTEST_TMP/tests/test-numinterval.sh
$AUTOPKGTEST_TMP/tests/test-numnormalize.sh
$AUTOPKGTEST_TMP/tests/test-numprocess.sh
$AUTOPKGTEST_TMP/tests/test-numrandom.sh
$AUTOPKGTEST_TMP/tests/test-numrange.sh
$AUTOPKGTEST_TMP/tests/test-numround.sh
$AUTOPKGTEST_TMP/tests/test-numsum.sh
