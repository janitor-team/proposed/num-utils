#!/bin/bash

# default test for numrandom
numrandom | numgrep /0..100/


# default test using parameters
myregex=/0..100/
numrandom $myregex | numgrep $myregex


# just another test
myregex=/-10000000..10000000/
numrandom $myregex | numgrep $myregex
