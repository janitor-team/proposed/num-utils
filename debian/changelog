num-utils (0.5-15) unstable; urgency=medium

  * New maintainer. (Closes: #918486)

 -- Regis Fernandes Gontijo <regisfg@gmail.com>  Tue, 09 Jun 2020 03:57:25 +0000

num-utils (0.5-14) unstable; urgency=medium

  * QA upload.
  * debian/control: added VCS fields.
  * debian/salsa-ci.yml: included to provide salsa CI tests.

 -- Regis Fernandes Gontijo <regisfg@gmail.com>  Fri, 05 Jun 2020 11:03:11 +0000

num-utils (0.5-13) unstable; urgency=medium

  * QA upload.
  * Set Debian QA Group as maintainer. (see #918486)
  * Using new DH level format. Consequently:
      - debian/compat: file no longer needed. Removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 13.
  * debian/control:
      - Added 'Multi-Arch: foreign'.
      - Added 'Rules-Requires-Root: binary-targets'.
      - Bumped Standards-Version to 4.5.0.
      - Fixed broken upstream URI in 'Homepage' field.
      - Removed unneeded variable ${shlibs:Depends}.
  * debian/copyright: migrated to Machine-readable 1.0 format.
  * debian/examples: install example tests from upstream.
  * debian/patches/:
      - 09_spelling-errors-in-upstream.diff: fix spelling errors in upstream.
      - 10_help_locks_stdin.diff: fix STDIN issues in upstream when calling
        executables with --help option.
      - 11_upstream_URI_manp.diff: Fix upstream URI in manpages.
      - Added a header for each patch.
  * debian/rules: removed debugging instructions.
  * debian/tests/: added automated CI tests for each executable.
  * debian/watch: fixed broken URI.

 -- Regis Fernandes Gontijo <regisfg@gmail.com>  Wed, 20 May 2020 02:26:24 +0000

num-utils (0.5-12) unstable; urgency=medium

  * Switched packaging style from cdbs to dh9.
  * debian/patches/05_numgrep_exit.diff: Added. Closes: #708308.
  * debian/patches/06_round_number.diff: Added. Closes: #714220.
  * debian/patches/07_denominator.diff: Added. Closes: #553913.
  * debian/patches/08_man_numsum.diff: Added. Closes: #566058.

 -- Bart Martens <bartm@debian.org>  Sat, 06 Aug 2016 09:11:22 +0200

num-utils (0.5-11) unstable; urgency=low

  * debian/patches/04_man.diff: Added.  Closes: #544867.
  * debian/watch: Updated.

 -- Bart Martens <bartm@debian.org>  Thu, 29 Oct 2009 20:17:31 +0100

num-utils (0.5-10) unstable; urgency=low

  * debian/patches/03_power.diff: Added.  Closes: #520589.
  * debian/control: Maintainer, Standards-Version, Homepage.

 -- Bart Martens <bartm@debian.org>  Tue, 14 Apr 2009 20:32:19 +0200

num-utils (0.5-9) unstable; urgency=low

  * debian/patches/02_die.diff: Added.  Closes: #429792.

 -- Bart Martens <bartm@knars.be>  Wed, 20 Jun 2007 12:28:47 +0200

num-utils (0.5-8) unstable; urgency=low

  * debian/patches/01_numsum_numeric.diff: Added.  Closes: #423667.

 -- Bart Martens <bartm@knars.be>  Sat, 16 Jun 2007 11:55:46 +0200

num-utils (0.5-7) unstable; urgency=low

  * numsum: Allow decimals and minus signs with -c and -r.  Closes: #410128.
  * numsum: Added function "decimals_friendly_sum".
  * debian/copyright: Updated.

 -- Bart Martens <bartm@knars.be>  Sat, 10 Feb 2007 09:01:28 +0100

num-utils (0.5-6) unstable; urgency=low

  * debian/*: Repackaged with cdbs.
  * debian/control: Fixed typo.  Closes: #390194.

 -- Bart Martens <bartm@knars.be>  Tue, 14 Nov 2006 17:14:37 +0100

num-utils (0.5-5) unstable; urgency=low

  * interval: Process value 0 correctly.  Closes: #380266.
  * average: Handling of empty list of numbers.  Closes: #377667.
  * average, bound, interval, normalize, numprocess, numsum, random, range,
    round, template: Removed needless repetition of text in man pages.
    Closes: #377638.

 -- Bart Martens <bartm@knars.be>  Sat, 29 Jul 2006 11:37:08 +0200

num-utils (0.5-4) unstable; urgency=low

  * range: Suppress warnings on stderr.  Closes: #376930.
  * average: Better documentation of numaverage -m.  Closes: #376928.
  * debian/control: Fixed typo.  Closes: #377312.

 -- Bart Martens <bartm@knars.be>  Sat,  8 Jul 2006 21:52:17 +0200

num-utils (0.5-3) unstable; urgency=low

  * average, bound, GOALS, interval, Makefile, normalize, numgrep, numprocess,
    numsum, random, range, README, round, template, debian/control,
    debian/rules: Added prefix "num" to programs.  No longer conflicts with
    gromacs.  Closes: #375935, #376268.
  * debian/rules: No longer installing MANIFEST.

 -- Bart Martens <bartm@knars.be>  Sat,  1 Jul 2006 21:28:08 +0200

num-utils (0.5-2) unstable; urgency=low

  * debian/rules: Fix documentation directory name.  Closes: #375771.  Patch
    by Justin B Rye <jbr@edlug.org.uk>, thanks.
  * debian/rules: Fixed lintian changelog-file-not-compressed.

 -- Bart Martens <bartm@knars.be>  Wed, 28 Jun 2006 23:02:46 +0200

num-utils (0.5-1) unstable; urgency=low

  * Initial release.  Closes: #358453.
  * debian/control: Conflicts with gromacs because of /usr/bin/average.
  * Makefile: Disabled rpm things, install in DESTDIR, and fixed lintian
    manpage-not-compressed-with-max-compression.

 -- Bart Martens <bartm@knars.be>  Sun, 11 Jun 2006 10:10:07 +0200
